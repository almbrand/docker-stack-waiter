# docker-stack-waiter

## Description
`docker-stack-waiter` was created to compensate for the [missing `--detach=false` option of the `docker stack deploy` command](https://github.com/docker/cli/issues/373), and is specifically built to be part of a CI/CD pipeline.

It monitors the status of the services in a docker-compose.yml file, and waits for them to be completely updated. If any of the services did not exist, `docker-stack-waiter` will wait for those services to reach the specified number of healthy replicas.

**Caveat:** Irrespective of this tool, it seems that if a service failes in the `paused` state due to a task failure, if you run `docker stack deploy` again without any changes, or if you run `docker service update --detach <service_name>`, the service will _always_ end up in the `completed` state, and thus `docker-stack-waiter` will report the service as successful. A workaround is to set the service's `deploy.update_config.failure_action` field to `rollback`.

## Installation

### Binary
Latest version can be downloaded from [here](https://gitlab.com/almbrand/docker-stack-waiter/-/jobs/artifacts/master/download?job=build-artifact)

### Docker image
```console
docker container run \
  --rm \
  --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock,readonly \
  --mount "type=bind,source=${PWD},target=${PWD},readonly" \
  --workdir "${PWD}" \
  registry.gitlab.com/almbrand/docker-stack-waiter [-deploy] <namespace>
```

## Usage
Run `docker-stack-waiter` like this:

```console
docker stack deploy -c docker-compose.yml <namespace> \
&& docker-stack-waiter <namespace>
```

or

```console
docker-stack-waiter -deploy <namespace>
```

The `-c` flag can be specified multiple times. The merged list of service names is used to find the services to wait for.

### Environment variables

`docker-stack-waiter` supports reading the `COMPSE_FILE` and `COMPOSE_PATH_SEPARATOR` variables to get the list of Docker Compose files to parse.

## Contributing
Issues and merge requests welcome. Please create an issue and create the merge request from that, as it keeps it possible to discuss the issues and their potential solutions separately.

## Credits
We basically ported the logic from [this GitHub comment](https://github.com/docker/cli/issues/373#issuecomment-413361678) by [@sorenhansendk](https://github.com/sorenhansendk) to Golang and added a few bits and pieces, so thanks for that.

Authors:
* Sune Keller (@sirlatrom)
* Loke Norlin Johannessen (@ablojh)

## License
The project is licensed under the [MIT license](/../blob/master/LICENSE).
